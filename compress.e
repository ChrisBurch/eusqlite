-- Euphoria object compression and decompression
--
-- Copied from database.e and modified so that it works with RAM and not with file.
-- Tone �koda, 26. August 2005

-- global constant DEBUG = 0, EXTRA_DEBUG = 0, APP_NAME = ""
-- include TSMisc.e -- TODO: remove, only for debugging (get_rand_eu_obj())

sequence Bytes -- read from here instead from file
integer Byte_pos -- instead of file position

-- Compressed format of Euphoria objects on disk
--
-- First byte:
--          0..248    -- immediate small integer, -9 to 239
		      -- since small negative integers -9..-1 might be common
constant I2B = 249,   -- 2-byte signed integer follows
	 I3B = 250,   -- 3-byte signed integer follows
	 I4B = 251,   -- 4-byte signed integer follows
	 F4B = 252,   -- 4-byte f.p. number follows
	 F8B = 253,   -- 8-byte f.p. number follows
	 S1B = 254,   -- sequence, 1-byte length follows, then elements
	 S4B = 255    -- sequence, 4-byte length follows, then elements

constant MIN1B = -9,
	 MAX1B = 239,
	 MIN2B = -power(2, 15),
	 MAX2B =  power(2, 15)-1,
	 MIN3B = -power(2, 23),
	 MAX3B =  power(2, 23)-1,
	 MIN4B = -power(2, 31)

-- emulation of getc(); this one reads from sequence and not from file.
-- this helps for easier translation of decompress_() to read from sequence and not from file.
-- i didn't need to modify decompress_() except change getc() to getc_() and
-- get4() to get4_() 
function getc_ ()
   integer c
   c = Bytes [Byte_pos]
   Byte_pos += 1
   return c
end function

atom mem0, mem1, mem2, mem3
mem0 = allocate(4)
mem1 = mem0 + 1
mem2 = mem0 + 2
mem3 = mem0 + 3

-- emulation of get4()
function get4_()
-- read 4-byte value at current position in sequence
    poke(mem0, getc_())
    poke(mem1, getc_())
    poke(mem2, getc_())
    poke(mem3, getc_())
    return peek4u(mem0)
end function

function decompress_(integer c)
    sequence s
    integer len
    
    if c = 0 then
	c = getc_()
	if c < I2B then
	    return c + MIN1B
	end if
    end if
    
    if c = I2B then
	return getc_() + 
	       #100 * getc_() +
	       MIN2B
    
    elsif c = I3B then
	return getc_() + 
	       #100 * getc_() + 
	       #10000 * getc_() +
	       MIN3B
    
    elsif c = I4B then
	return get4_() + MIN4B
    
    elsif c = F4B then
	return float32_to_atom({getc_(), getc_(), 
				getc_(), getc_()})
    elsif c = F8B then
	return float64_to_atom({getc_(), getc_(),
				getc_(), getc_(),
				getc_(), getc_(),
				getc_(), getc_()})
    else
	-- sequence
	if c = S1B then
	    len = getc_()
	else
	    len = get4_()
	end if
	s = repeat(0, len)
	for i = 1 to len do
	    -- in-line small integer for greater speed on strings
	    c = getc_()
	    if c < I2B then
		s[i] = c + MIN1B
	    else
		s[i] = decompress_(c)
	    end if
	end for
	return s
    end if
end function

global function decompress (sequence bytes)
-- decompress a sequence of bytes which should represent
-- a Euphoria object, and return it
   Bytes = bytes
   Byte_pos = 1
   return decompress_ (0)
end function

global function compress(object x)
-- return the compressed representation of a Euphoria object 
-- as a sequence of bytes
    sequence x4, s
    
    if integer(x) then
	if x >= MIN1B and x <= MAX1B then
	    return {x - MIN1B}
	    
	elsif x >= MIN2B and x <= MAX2B then
	    x -= MIN2B
	    return {I2B, and_bits(x, #FF), floor(x / #100)}
	    
	elsif x >= MIN3B and x <= MAX3B then
	    x -= MIN3B
	    return {I3B, and_bits(x, #FF), and_bits(floor(x / #100), #FF), floor(x / #10000)}
	    
	else
	    return I4B & int_to_bytes(x-MIN4B)    
	    
	end if
    
    elsif atom(x) then
	-- floating point
	x4 = atom_to_float32(x)
	if x = float32_to_atom(x4) then
	    -- can represent as 4-byte float
	    return F4B & x4
	else
	    return F8B & atom_to_float64(x)
	end if

    else
	-- sequence
	if length(x) <= 255 then
	    s = {S1B, length(x)}
	else
	    s = S4B & int_to_bytes(length(x))
	end if  
	for i = 1 to length(x) do
	    s &= compress(x[i])
	end for
	return s
    end if
end function

-- procedure test_compress_decompress ()
--     sequence source, compressed_string, decompressed
--     for i = 1 to 10000 do
--         -- source = {i, 0, time () * rand (10000),
--             -- {"this is blob data"}, {8, {9, 10}}}
--         source = get_rand_eu_obj (1000)
--         compressed_string = compress (source)
--         decompressed = decompress (compressed_string)
--         if not equal (source, decompressed) then
--             puts (1, "ASSERT compress/decompress\n")
--             ?1/0
--         end if
--     end for
--     puts (1, "compress/decompress works correct\n")
--     if wait_key () then end if
-- end procedure
-- test_compress_decompress ()