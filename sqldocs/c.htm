<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!-- saved from url=(0047)http://www.hwaci.com/sw/sqlite/c_interface.html -->
<HTML><HEAD><TITLE>The C language interface to the SQLite library</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<META content="MSHTML 5.50.4611.1300" name=GENERATOR></HEAD>
<BODY bgColor=white>
<H1 align=center>The C language interface to the SQLite library </H1>
<P align=center>(This page was last modified on 2002/01/16 21:00:28 UTC) </P>
<P>The SQLite library is designed to be very easy to use from a C or C++ 
program. This document gives an overview of the C/C++ programming interface.</P>
<H2>The Core API</H2>
<P>The interface to the SQLite library consists of three core functions, one 
opaque data structure, and some constants used as return values. The core 
interface is as follows:</P>
<BLOCKQUOTE><PRE>typedef struct sqlite sqlite;
#define SQLITE_OK           0   /* Successful result */

sqlite *sqlite_open(const char *dbname, int mode, char **errmsg);

void sqlite_close(sqlite*);

int sqlite_exec(
  sqlite*,
  char *sql,
  int (*)(void*,int,char**,char**),
  void*,
  char **errmsg
);
</PRE></BLOCKQUOTE>
<P>The above is all you really need to know in order to use SQLite in your C or 
C++ programs. There are other convenience functions available (and described 
below) but we will begin by describing the core functions shown above. </P>
<H2>Opening a database</H2>
<P>Use the <B>sqlite_open()</B> function to open an existing SQLite database or 
to create a new SQLite database. The first argument is the database name. The 
second argument is intended to signal whether the database is going to be used 
for reading and writing or just for reading. But in the current implementation, 
the second argument to <B>sqlite_open</B> is ignored. The third argument is a 
pointer to a string pointer. If the third argument is not NULL and an error 
occurs while trying to open the database, then an error message will be written 
to memory obtained from malloc() and *errmsg will be made to point to this error 
message. The calling function is responsible for freeing the memory when it has 
finished with it.</P>
<P>The name of an SQLite database is the name of a file that will contain the 
database. If the file does not exist, SQLite attempts to create and initialize 
it. If the file is read-only (due to permission bits or because it is located on 
read-only media like a CD-ROM) then SQLite opens the database for reading only. 
The entire SQL database is stored in a single file on the disk. But additional 
temporary files may be created during the execution of an SQL command in order 
to store the database rollback journal or temporary and intermediate results of 
a query.</P>
<P>The return value of the <B>sqlite_open()</B> function is a pointer to an 
opaque <B>sqlite</B> structure. This pointer will be the first argument to all 
subsequent SQLite function calls that deal with the same database. NULL is 
returned if the open fails for any reason.</P>
<H2>Closing the database</H2>
<P>To close an SQLite database, call the <B>sqlite_close()</B> function passing 
it the sqlite structure pointer that was obtained from a prior call to 
<B>sqlite_open</B>. If a transaction is active when the database is closed, the 
transaction is rolled back.</P>
<H2>Executing SQL statements</H2>
<P>The <B>sqlite_exec()</B> function is used to process SQL statements and 
queries. This function requires 5 parameters as follows:</P>
<OL>
  <LI>
  <P>A pointer to the sqlite structure obtained from a prior call to 
  <B>sqlite_open()</B>.</P>
  <LI>
  <P>A null-terminated string containing the text of one or more SQL statements 
  and/or queries to be processed.</P>
  <LI>
  <P>A pointer to a callback function which is invoked once for each row in the 
  result of a query. This argument may be NULL, in which case no callbacks will 
  ever be invoked.</P>
  <LI>
  <P>A pointer that is forwarded to become the first argument to the callback 
  function.</P>
  <LI>
  <P>A pointer to an error string. Error messages are written to space obtained 
  from malloc() and the error string is made to point to the malloced space. The 
  calling function is responsible for freeing this space when it has finished 
  with it. This argument may be NULL, in which case error messages are not 
  reported back to the calling function.</P></LI></OL>
<P>The callback function is used to receive the results of a query. A prototype 
for the callback function is as follows:</P>
<BLOCKQUOTE><PRE>int Callback(void *pArg, int argc, char **argv, char **columnNames){
  return 0;
}
</PRE></BLOCKQUOTE>
<P>The first argument to the callback is just a copy of the fourth argument to 
<B>sqlite_exec()</B> This parameter can be used to pass arbitrary information 
through to the callback function from client code. The second argument is the 
number of columns in the query result. The third argument is an array of 
pointers to strings where each string is a single column of the result for that 
record. Note that the callback function reports a NULL value in the database as 
a NULL pointer, which is very different from an empty string. If the i-th 
parameter is an empty string, we will get:</P>
<BLOCKQUOTE><PRE>argv[i][0] == 0
</PRE></BLOCKQUOTE>
<P>But if the i-th parameter is NULL we will get:</P>
<BLOCKQUOTE><PRE>argv[i] == 0
</PRE></BLOCKQUOTE>
<P>The names of the columns are contained in the fourth argument.</P>
<P>If the EMPTY_RESULT_CALLBACKS pragma is set to ON and the result of a query 
is an empty set, then the callback is invoked once with the third parameter 
(argv) set to 0. In other words 
<BLOCKQUOTE><PRE>argv == 0
</PRE></BLOCKQUOTE>The second parameter (argc) and the fourth parameter 
(columnNames) are still valid and can be used to determine the number and names 
of the result columns if there had been a result. The default behavior is not to 
invoke the callback at all if the result set is empty.
<P></P>
<P>The callback function should normally return 0. If the callback function 
returns non-zero, the query is immediately aborted and <B>sqlite_exec()</B> will 
return SQLITE_ABORT.</P>
<H2>Error Codes</H2>
<P>The <B>sqlite_exec()</B> function normally returns SQLITE_OK. But if 
something goes wrong it can return a different value to indicate the type of 
error. Here is a complete list of the return codes: </P>
<BLOCKQUOTE><PRE>#define SQLITE_OK           0   /* Successful result */
#define SQLITE_ERROR        1   /* SQL error or missing database */
#define SQLITE_INTERNAL     2   /* An internal logic error in SQLite */
#define SQLITE_PERM         3   /* Access permission denied */
#define SQLITE_ABORT        4   /* Callback routine requested an abort */
#define SQLITE_BUSY         5   /* The database file is locked */
#define SQLITE_LOCKED       6   /* A table in the database is locked */
#define SQLITE_NOMEM        7   /* A malloc() failed */
#define SQLITE_READONLY     8   /* Attempt to write a readonly database */
#define SQLITE_INTERRUPT    9   /* Operation terminated by sqlite_interrupt() */
#define SQLITE_IOERR       10   /* Some kind of disk I/O error occurred */
#define SQLITE_CORRUPT     11   /* The database disk image is malformed */
#define SQLITE_NOTFOUND    12   /* (Internal Only) Table or record not found */
#define SQLITE_FULL        13   /* Insertion failed because database is full */
#define SQLITE_CANTOPEN    14   /* Unable to open the database file */
#define SQLITE_PROTOCOL    15   /* Database lock protocol error */
#define SQLITE_EMPTY       16   /* (Internal Only) Database table is empty */
#define SQLITE_SCHEMA      17   /* The database schema changed */
#define SQLITE_TOOBIG      18   /* Too much data for one row of a table */
#define SQLITE_CONSTRAINT  19   /* Abort due to contraint violation */
#define SQLITE_MISMATCH    20   /* Data type mismatch */
</PRE></BLOCKQUOTE>
<P>The meanings of these various return values are as follows: </P>
<BLOCKQUOTE>
  <DL>
    <DT>SQLITE_OK 
    <DD>
    <P>This value is returned if everything worked and there were no errors. 
</P>
    <DT>SQLITE_INTERNAL 
    <DD>
    <P>This value indicates that an internal consistency check within the SQLite 
    library failed. This can only happen if there is a bug in the SQLite 
    library. If you ever get an SQLITE_INTERNAL reply from an 
    <B>sqlite_exec()</B> call, please report the problem on the SQLite mailing 
    list. </P>
    <DT>SQLITE_ERROR 
    <DD>
    <P>This return value indicates that there was an error in the SQL that was 
    passed into the <B>sqlite_exec()</B>. </P>
    <DT>SQLITE_PERM 
    <DD>
    <P>This return value says that the access permissions on the database file 
    are such that the file cannot be opened. </P>
    <DT>SQLITE_ABORT 
    <DD>
    <P>This value is returned if the callback function returns non-zero. </P>
    <DT>SQLITE_BUSY 
    <DD>
    <P>This return code indicates that another program or thread has the 
    database locked. SQLite allows two or more threads to read the database at 
    the same time, but only one thread can have the database open for writing at 
    the same time. Locking in SQLite is on the entire database.</P>
    <P></P>
    <DT>SQLITE_LOCKED 
    <DD>
    <P>This return code is similar to SQLITE_BUSY in that it indicates that the 
    database is locked. But the source of the lock is a recursive call to 
    <B>sqlite_exec()</B>. This return can only occur if you attempt to invoke 
    sqlite_exec() from within a callback routine of a query from a prior 
    invocation of sqlite_exec(). Recursive calls to sqlite_exec() are allowed as 
    long as they do not attempt to write the same table. </P>
    <DT>SQLITE_NOMEM 
    <DD>
    <P>This value is returned if a call to <B>malloc()</B> fails. </P>
    <DT>SQLITE_READONLY 
    <DD>
    <P>This return code indicates that an attempt was made to write to a 
    database file that is opened for reading only. </P>
    <DT>SQLITE_INTERRUPT 
    <DD>
    <P>This value is returned if a call to <B>sqlite_interrupt()</B> interrupts 
    a database operation in progress. </P>
    <DT>SQLITE_IOERR 
    <DD>
    <P>This value is returned if the operating system informs SQLite that it is 
    unable to perform some disk I/O operation. This could mean that there is no 
    more space left on the disk. </P>
    <DT>SQLITE_CORRUPT 
    <DD>
    <P>This value is returned if SQLite detects that the database it is working 
    on has become corrupted. Corruption might occur due to a rogue process 
    writing to the database file or it might happen due to an perviously 
    undetected logic error in of SQLite. This value is also returned if a disk 
    I/O error occurs in such a way that SQLite is forced to leave the database 
    file in a corrupted state. The latter should only happen due to a hardware 
    or operating system malfunction. </P>
    <DT>SQLITE_FULL 
    <DD>
    <P>This value is returned if an insertion failed because there is no space 
    left on the disk, or the database is too big to hold any more information. 
    The latter case should only occur for databases that are larger than 2GB in 
    size. </P>
    <DT>SQLITE_CANTOPEN 
    <DD>
    <P>This value is returned if the database file could not be opened for some 
    reason. </P>
    <DT>SQLITE_PROTOCOL 
    <DD>
    <P>This value is returned if some other process is messing with file locks 
    and has violated the file locking protocol that SQLite uses on its rollback 
    journal files. </P>
    <DT>SQLITE_SCHEMA 
    <DD>
    <P>When the database first opened, SQLite reads the database schema into 
    memory and uses that schema to parse new SQL statements. If another process 
    changes the schema, the command currently being processed will abort because 
    the virtual machine code generated assumed the old schema. This is the 
    return code for such cases. Retrying the command usually will clear the 
    problem. </P>
    <DT>SQLITE_TOOBIG 
    <DD>
    <P>SQLite will not store more than about 1 megabyte of data in a single row 
    of a single table. If you attempt to store more than 1 megabyte in a single 
    row, this is the return code you get. </P>
    <DT>SQLITE_CONSTRAINT 
    <DD>
    <P>This constant is returned if the SQL statement would have violated a 
    database constraint. </P>
    <DT>SQLITE_MISMATCH 
    <DD>
    <P>This error occurs when there is an attempt to insert non-integer data 
    into a column labeled INTEGER PRIMARY KEY. For most columns, SQLite ignores 
    the data type and allows any kind of data to be stored. But an INTEGER 
    PRIMARY KEY column is only allowed to store integer data. 
</P></DD></DL></BLOCKQUOTE>
<H2>The Extended API</H2>
<P>Only the three core routines shown above are required to use SQLite. But 
there are many other functions that provide useful interfaces. These extended 
routines are as follows: </P>
<BLOCKQUOTE><PRE>int sqlite_last_insert_rowid(sqlite*);

int sqlite_get_table(
  sqlite*,
  char *sql,
  char ***result,
  int *nrow,
  int *ncolumn,
  char **errmsg
);

void sqlite_free_table(char**);

void sqlite_interrupt(sqlite*);

int sqlite_complete(const char *sql);

void sqlite_busy_handler(sqlite*, int (*)(void*,const char*,int), void*);

void sqlite_busy_timeout(sqlite*, int ms);

const char sqlite_version[];

const char sqlite_encoding[];

int sqlite_exec_printf(
  sqlite*,
  char *sql,
  int (*)(void*,int,char**,char**),
  void*,
  char **errmsg,
  ...
);

int sqlite_exec_vprintf(
  sqlite*,
  char *sql,
  int (*)(void*,int,char**,char**),
  void*,
  char **errmsg,
  va_list
);

int sqlite_get_table_printf(
  sqlite*,
  char *sql,
  char ***result,
  int *nrow,
  int *ncolumn,
  char **errmsg,
  ...
);

int sqlite_get_table_vprintf(
  sqlite*,
  char *sql,
  char ***result,
  int *nrow,
  int *ncolumn,
  char **errmsg,
  va_list
);

</PRE></BLOCKQUOTE>
<P>All of the above definitions are included in the "sqlite.h" header file that 
comes in the source tree.</P>
<H2>The ROWID of the most recent insert</H2>
<P>Every row of an SQLite table has a unique integer key. If the table has a 
column labeled INTEGER PRIMARY KEY, then that column servers as the key. If 
there is no INTEGER PRIMARY KEY column then the key is a random integer. The key 
for a row can be accessed in a SELECT statement or used in a WHERE or ORDER BY 
clause using any of the names "ROWID", "OID", or "_ROWID_".</P>
<P>When you do an insert into a table that does not have an INTEGER PRIMARY KEY 
column, or if the table does have an INTEGER PRIMARY KEY but the value for that 
column is not specified in the VALUES clause of the insert, then the key is 
automatically generated. You can find the value of the key for the most recent 
INSERT statement using the <B>sqlite_last_insert_rowid()</B> API function.</P>
<H2>Querying without using a callback function</H2>
<P>The <B>sqlite_get_table()</B> function is a wrapper around 
<B>sqlite_exec()</B> that collects all the information from successive callbacks 
and write it into memory obtained from malloc(). This is a convenience function 
that allows the application to get the entire result of a database query with a 
single function call.</P>
<P>The main result from <B>sqlite_get_table()</B> is an array of pointers to 
strings. There is one element in this array for each column of each row in the 
result. NULL results are represented by a NULL pointer. In addition to the 
regular data, there is an added row at the beginning of the array that contains 
the names of each column of the result.</P>
<P>As an example, consider the following query:</P>
<BLOCKQUOTE>SELECT employee_name, login, host FROM users WHERE logic LIKE 
  'd%'; </BLOCKQUOTE>
<P>This query will return the name, login and host computer name for every 
employee whose login begins with the letter "d". If this query is submitted to 
<B>sqlite_get_table()</B> the result might look like this:</P>
<BLOCKQUOTE>nrow = 2<BR>ncolumn = 3<BR>result[0] = 
  "employee_name"<BR>result[1] = "login"<BR>result[2] = "host"<BR>result[3] = 
  "dummy"<BR>result[4] = "No such user"<BR>result[5] = 0<BR>result[6] = "D. 
  Richard Hipp"<BR>result[7] = "drh"<BR>result[8] = "zadok" </BLOCKQUOTE>
<P>Notice that the "host" value for the "dummy" record is NULL so the result[] 
array contains a NULL pointer at that slot.</P>
<P>If the result set of a query is empty, then by default 
<B>sqlite_get_table()</B> will set nrow to 0 and leave its result parameter is 
set to NULL. But if the EMPTY_RESULT_CALLBACKS pragma is ON then the result 
parameter is initialized to the names of the columns only. For example, consider 
this query which has an empty result set:</P>
<BLOCKQUOTE>SELECT employee_name, login, host FROM users WHERE employee_name 
  IS NULL; </BLOCKQUOTE>
<P>The default behavior gives this results: </P>
<BLOCKQUOTE>nrow = 0<BR>ncolumn = 0<BR>result = 0<BR></BLOCKQUOTE>
<P>But if the EMPTY_RESULT_CALLBACKS pragma is ON, then the following is 
returned: </P>
<BLOCKQUOTE>nrow = 0<BR>ncolumn = 3<BR>result[0] = 
  "employee_name"<BR>result[1] = "login"<BR>result[2] = "host"<BR></BLOCKQUOTE>
<P>Memory to hold the information returned by <B>sqlite_get_table()</B> is 
obtained from malloc(). But the calling function should not try to free this 
information directly. Instead, pass the complete table to 
<B>sqlite_free_table()</B> when the table is no longer needed. It is safe to 
call <B>sqlite_free_table()</B> with a NULL pointer such as would be returned if 
the result set is empty.</P>
<P>The <B>sqlite_get_table()</B> routine returns the same integer result code as 
<B>sqlite_exec()</B>.</P>
<H2>Interrupting an SQLite operation</H2>
<P>The <B>sqlite_interrupt()</B> function can be called from a different thread 
or from a signal handler to cause the current database operation to exit at its 
first opportunity. When this happens, the <B>sqlite_exec()</B> routine (or the 
equivalent) that started the database operation will return 
SQLITE_INTERRUPT.</P>
<H2>Testing for a complete SQL statement</H2>
<P>The next interface routine to SQLite is a convenience function used to test 
whether or not a string forms a complete SQL statement. If the 
<B>sqlite_complete()</B> function returns true when its input is a string, then 
the argument forms a complete SQL statement. There are no guarantees that the 
syntax of that statement is correct, but we at least know the statement is 
complete. If <B>sqlite_complete()</B> returns false, then more text is required 
to complete the SQL statement.</P>
<P>For the purpose of the <B>sqlite_complete()</B> function, an SQL statement is 
complete if it ends in a semicolon.</P>
<P>The <B>sqlite</B> command-line utility uses the <B>sqlite_complete()</B> 
function to know when it needs to call <B>sqlite_exec()</B>. After each line of 
input is received, <B>sqlite</B> calls <B>sqlite_complete()</B> on all input in 
its buffer. If <B>sqlite_complete()</B> returns true, then <B>sqlite_exec()</B> 
is called and the input buffer is reset. If <B>sqlite_complete()</B> returns 
false, then the prompt is changed to the continuation prompt and another line of 
text is read and added to the input buffer.</P>
<H2>Library version string</H2>
<P>The SQLite library exports the string constant named <B>sqlite_version</B> 
which contains the version number of the library. The header file contains a 
macro SQLITE_VERSION with the same information. If desired, a program can 
compare the SQLITE_VERSION macro against the <B>sqlite_version</B> string 
constant to verify that the version number of the header file and the library 
match.</P>
<H2>Library character encoding</H2>
<P>By default, SQLite assumes that all data uses a fixed-size 8-bit character 
(iso8859). But if you give the --enable-utf8 option to the configure script, 
then the library assumes UTF-8 variable sized characters. This makes a 
difference for the LIKE and GLOB operators and the LENGTH() and SUBSTR() 
functions. The static string <B>sqlite_encoding</B> will be set to either 
"UTF-8" or "iso8859" to indicate how the library was compiled. In addition, the 
<B>sqlite.h</B> header file will define one of the macros <B>SQLITE_UTF8</B> or 
<B>SQLITE_ISO8859</B>, as appropriate.</P>
<P>Note that the character encoding mechanism used by SQLite cannot be changed 
at run-time. This is a compile-time option only. The <B>sqlite_encoding</B> 
character string just tells you how the library was compiled.</P>
<H2>Changing the library's response to locked files</H2>
<P>The <B>sqlite_busy_handler()</B> procedure can be used to register a busy 
callback with an open SQLite database. The busy callback will be invoked 
whenever SQLite tries to access a database that is locked. The callback will 
typically do some other useful work, or perhaps sleep, in order to give the lock 
a chance to clear. If the callback returns non-zero, then SQLite tries again to 
access the database and the cycle repeats. If the callback returns zero, then 
SQLite aborts the current operation and returns SQLITE_BUSY.</P>
<P>The arguments to <B>sqlite_busy_handler()</B> are the opaque structure 
returned from <B>sqlite_open()</B>, a pointer to the busy callback function, and 
a generic pointer that will be passed as the first argument to the busy 
callback. When SQLite invokes the busy callback, it sends it three arguments: 
the generic pointer that was passed in as the third argument to 
<B>sqlite_busy_handler</B>, the name of the database table or index that the 
library is trying to access, and the number of times that the library has 
attempted to access the database table or index.</P>
<P>For the common case where we want the busy callback to sleep, the SQLite 
library provides a convenience routine <B>sqlite_busy_timeout()</B>. The first 
argument to <B>sqlite_busy_timeout()</B> is a pointer to an open SQLite database 
and the second argument is a number of milliseconds. After 
<B>sqlite_busy_timeout()</B> has been executed, the SQLite library will wait for 
the lock to clear for at least the number of milliseconds specified before it 
returns SQLITE_BUSY. Specifying zero milliseconds for the timeout restores the 
default behavior.</P>
<H2>Using the <TT>_printf()</TT> wrapper functions</H2>
<P>The four utility functions</P>
<P>
<UL>
  <LI><B>sqlite_exec_printf()</B> 
  <LI><B>sqlite_exec_vprintf()</B> 
  <LI><B>sqlite_get_table_printf()</B> 
  <LI><B>sqlite_get_table_vprintf()</B> </LI></UL>
<P></P>
<P>implement the same query functionality as <B>sqlite_exec()</B> and 
<B>sqlite_get_table()</B>. But instead of taking a complete SQL statement as 
their second argument, the four <B>_printf</B> routines take a printf-style 
format string. The SQL statement to be executed is generated from this format 
string and from whatever additional arguments are attached to the end of the 
function call.</P>
<P>There are two advantages to using the SQLite printf functions instead of 
<B>sprintf()</B>. First of all, with the SQLite printf routines, there is never 
a danger of overflowing a static buffer as there is with <B>sprintf()</B>. The 
SQLite printf routines automatically allocate (and later free) as much memory as 
is necessary to hold the SQL statements generated.</P>
<P>The second advantage the SQLite printf routines have over <B>sprintf()</B> is 
a new formatting option specifically designed to support string literals in SQL. 
Within the format string, the %q formatting option works very much like %s in 
that it reads a null-terminated string from the argument list and inserts it 
into the result. But %q translates the inserted string by making two copies of 
every single-quote (') character in the substituted string. This has the effect 
of escaping the end-of-string meaning of single-quote within a string literal. 
</P>
<P>Consider an example. Suppose you are trying to insert a string value into a 
database table where the string value was obtained from user input. Suppose the 
string to be inserted is stored in a variable named zString. The code to do the 
insertion might look like this:</P>
<BLOCKQUOTE><PRE>sqlite_exec_printf(db,
  "INSERT INTO table1 VALUES('%s')",
  0, 0, 0, zString);
</PRE></BLOCKQUOTE>
<P>If the zString variable holds text like "Hello", then this statement will 
work just fine. But suppose the user enters a string like "Hi y'all!". The SQL 
statement generated reads as follows: 
<BLOCKQUOTE><PRE>INSERT INTO table1 VALUES('Hi y'all')
</PRE></BLOCKQUOTE>
<P>This is not valid SQL because of the apostrophy in the word "y'all". But if 
the %q formatting option is used instead of %s, like this:</P>
<BLOCKQUOTE><PRE>sqlite_exec_printf(db,
  "INSERT INTO table1 VALUES('%q')",
  0, 0, 0, zString);
</PRE></BLOCKQUOTE>
<P>Then the generated SQL will look like the following:</P>
<BLOCKQUOTE><PRE>INSERT INTO table1 VALUES('Hi y''all')
</PRE></BLOCKQUOTE>
<P>Here the apostrophy has been escaped and the SQL statement is well-formed. 
When generating SQL on-the-fly from data that might contain a single-quote 
character ('), it is always a good idea to use the SQLite printf routines and 
the %q formatting option instead of <B>sprintf</B>. </P>
<H2>Usage Examples</H2>
<P>For examples of how the SQLite C/C++ interface can be used, refer to the 
source code for the <B>sqlite</B> program in the file <B>src/shell.c</B> of the 
source tree. Additional information about sqlite is available at <A 
href="http://www.hwaci.com/sw/sqlite/sqlite.html">sqlite.html</A>. See also the 
sources to the Tcl interface for SQLite in the source file 
<B>src/tclsqlite.c</B>.</P>
<P>
<HR>

<P></P>
<P><A href="http://www.hwaci.com/sw/sqlite/index.html"><IMG 
src="goback.jpeg" border=0> 
Back to the SQLite Home Page</A> </P></BODY></HTML>
