
	    -- fields for Archive Database

global constant
    A_PLATFORM = 1,   -- numeric: 0 = GEN, 1 = DOS, 2 = WIN, 3 = LNX
    A_HREF = 2,       -- string
    A_TITLE = 3,      -- string
    A_SIZE = 4,       -- number of K bytes
    A_NAME = 5,       -- string
    A_YEAR = 6,       -- year: 4 digits
    A_MONTH = 7,      -- month: 1 to 12      
    A_DAY = 8,        -- day of the month: 1 to 31
    A_MONEY = 9,      -- integer - number of pennies
    A_DESCRIPTION = 10, -- one string with \n's
    A_UPDATED = 11,     -- 0 (not updated) or 1 (updated)
    A_STAMPED = 12,    -- 0 (not stamped) or 1 (stamped)
    A_CATEGORY = 13    -- html filename without ".htm"

